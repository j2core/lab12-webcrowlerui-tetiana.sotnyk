<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 3/16/17
  Time: 5:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Change User Password</title>
</head>
<body>
<%@include file="header.jsp" %>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>User's menu:</h4>
    <a href="profile.jsp"> Profile</a>
    <a href="changeuserdata.jsp">Change user's information</a>
    <a style="background-color: lightgray" href="changeuserpass.jsp">Change user's password</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <c:if test="${sessionScope.changedPass != null}">

            <p style="color: red"> You have mistake in passwords. Please try again. </p>

        </c:if>

        <h3>
            Change user password:
        </h3>

        <form action="ChangeUserPassServlet" method="post">

            <input type="hidden" name="mySecurityToken" value="${sessionScope.securityToken}"/>

            <h4>New Information:</h4>

            Enter new password:<br>
            <input type="password" name="new1Password"/><br>
            Enter new password again:<br>
            <input type="password" name="new2Password"/><br><br>

            <h4>Old information:</h4>

            Name: <p style="font-weight:900">${sessionScope.securityToken.getUserData().getUserName()}</p>
            Enter your login:<br>
            <input type="text" name="login"/><br>
            Enter old password:<br>
            <input type="password" name="oldPassword"/><br><br>

            <input type="submit" value="submit"/>
        </form>

</div>

<%@include file="footer.jsp" %>
</body>
</html>


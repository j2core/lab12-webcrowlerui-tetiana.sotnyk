<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
</head>
<body>
<%@include file="header.jsp" %>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <br>
    <a style="background-color: lightgray" href="#"> Home</a>
    <h4>Statistics:</h4>
    <a href="statistic.jsp">Web Crawler's statistics</a>
    <a href="nodestatistics.jsp">Node's statistics</a>
    <a href="userstatistics.jsp">User's statistics</a>
    <h4>Management:</h4>
    <a href="nodesmanage.jsp">Manage Web Crawler's nodes</a>
    <a href="usersmanage.jsp">Manage user's information</a>
    <br>
    <a href="LogoutServlet">Logout</a>

</nav>

<div class="w3-container" style="margin-left: 17%">

    <c:if test="${sessionScope.verifyLogin == null}">
        <br>
        <h3>
            To get started, you need to enter your login and password.
        </h3>

        <form action="LoginServlet" method="post">
            Enter your login:<br>
            <input type="text" name="login"/><br>
            Enter your password:<br>
            <input type="password" name="password"/><br><br>
            <input type="submit" value="submit"/>
        </form>
    </c:if>
    <c:if test="${sessionScope.verifyLogin == false}">

        <br>
        <h3 style="color: red">
            Sorry! Your login or password was incorrect, or does not exist in DB.
        </h3>
        <p style="color: red"> Please try again.</p>

        <form action="LoginServlet" method="post">
            Enter your login:<br>
            <input type="text" name="login"/><br>
            Enter your password:<br>
            <input type="password" name="password"/><br><br>
            <input type="submit" value="submit"/>
        </form>

    </c:if>
    <c:if test="${sessionScope.securityToken != null}" >

        <h2> Welcome to the Web Crawler! </h2>
        <br>
        <p>Main idea of this task is to create high-performance web-crawler, backed by database.
            Crawler itself is a simple tool which starts from some starting URL, downloads page parse links (local and external)
            parses page content, saves page content processed to database saves links as a new starting points for later processing.
        </p>

    </c:if>

</div>
<%@include file="footer.jsp" %>
</body>
</html>

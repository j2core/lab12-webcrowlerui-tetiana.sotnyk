<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 2/10/17
  Time: 12:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Node's statistics</title>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Statistics:</h4>
    <a href="statistic.jsp">Web Crawler statistics</a>
    <a style="background-color: lightgray" href="#">Node's statistics</a>
    <a href="userstatistics.jsp">User's statistics</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <br>

        <form action="NodesStatisticsServlet" method="post">
            Enter node's Id which statistics do you want see:<br>
            <input type="text" name="nodeId"/><br><br>
            <input type="submit" value="submit"/>
        </form>

        <c:if test="${sessionScope.nodeId != null}">

            <p> Your node's Id is ${sessionScope.nodeId}.</p>

            <sql:query dataSource="${snapshot}" var="existNodeId">
                SELECT * from nodeData where nodeId = ?
                <sql:param value="${sessionScope.nodeId}"/>
            </sql:query>

            <c:if test="${existNodeId.rowCount == 1}">

                <c:forEach var="row" items="${existNodeId.rows}">

                    <sql:query dataSource="${snapshot}" var="urlProcessed">
                        SELECT * from urlData where nodeId = ? and status = 2
                        <sql:param value="${sessionScope.nodeId}"/>
                    </sql:query>

                    <sql:query dataSource="${snapshot}" var="urlProcesses">
                        SELECT * from urlData where nodeId = ? and status = 1
                        <sql:param value="${sessionScope.nodeId}"/>
                    </sql:query>

                    <table border="2" width="80%">
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>

                        <tr>

                            <td><c:out value="Node's start time"></c:out></td>
                            <jsp:useBean id="startDate" class="java.util.Date"/>
                            <jsp:setProperty name="startDate" property="time" value="${row.startUnixTime}"/>
                            <td><fmt:formatDate value="${startDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>

                        </tr>
                        <tr>

                            <td><c:out value="Node's stop time"></c:out></td>
                            <c:if test="${row.stopUnixTime > 0}">
                                <jsp:useBean id="stopDate" class="java.util.Date"/>
                                <jsp:setProperty name="stopDate" property="time" value="${row.stopUnixTime}"/>
                                <td><fmt:formatDate value="${stopDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>
                            </c:if>
                            <c:if test="${row.stopUnixTime == 0}">
                                <td><c:out value="00:00:00"/></td>
                            </c:if>
                        </tr>
                        <tr>

                            <td><c:out value="Amount processed URLs"></c:out></td>
                            <td><c:out value="${urlProcessed.rowCount}"></c:out></td>

                        </tr>
                        <tr>

                            <td><c:out value="Amount processes URLs"></c:out></td>
                            <td><c:out value="${urlProcesses.rowCount}"></c:out></td>

                        </tr>
                    </table>
                </c:forEach>

            </c:if>

            <c:if test="${existNodeId.rowCount < 1}">

                <p style="color: red">

                    Sorry this node's id isn't exist in data base. Please try again.
                </p>

            </c:if>

        </c:if>

        <c:if test="${sessionScope.correctId == false}">

            <p style="color: red">

                Sorry! Your text isn't correct node's id. Please try again.
            </p>

        </c:if>

        <br><br>
        <form action="statistic.jsp">
            <button type="submit"> Return to the web crawler statistics </button>
        </form>

</div>

<%@include file="footer.jsp" %>
</body>
</html>

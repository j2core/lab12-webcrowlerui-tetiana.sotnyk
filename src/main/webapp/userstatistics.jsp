<%@ page import="com.j2core.sts.webcrawler.ui.dto.UserRole" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 1/24/17
  Time: 4:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP Page</title>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Statistics:</h4>
    <a href="statistic.jsp">Web Crawler statistics</a>
    <a href="nodestatistics.jsp">Node's statistics</a>
    <a style="background-color: lightgray" href="userstatistics.jsp">User's statistics</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

    <sql:query dataSource="${snapshot}" var="userData">
    SELECT * from UserData;
    </sql:query>

    <h2> Users statistics: </h2>
    <table border="2" width="80%">
        <tr>
            <th>User ID</th>
            <th>User's Name</th>
            <th>User's Login</th>
            <th>Roles</th>
        </tr>
        <c:forEach var="row" items="${userData.rows}">

            <sql:query dataSource="${snapshot}" var="roles">
                SELECT * from permission where userId = ?
                <sql:param value="${row.userId}"/>
            </sql:query>

            <tr>
                <td><c:out value="${row.userId}"/></td>
                <td><c:out value="${row.userName}"/></td>
                <td><c:out value="${row.login}"/></td>

                <c:set var="admin" value="<%=UserRole.ADMIN%>" scope="page"/>
                <c:set var="user" value="<%=UserRole.USER%>" scope="page"/>
                <c:set var="guest" value="<%=UserRole.GUEST%>" scope="page"/>

                <c:set var="rolesList" value=""/>
                <c:forEach var="rowRoles" items="${roles.rows}">

                    <c:choose>
                        <c:when test="${rowRoles.groupId eq admin.roleId}">
                            <c:set var="nameRole" value="${admin.roleName}"/>
                        </c:when>
                        <c:when test="${rowRoles.groupId eq user.roleId}">
                            <c:set var="nameRole" value="${user.roleName}"/>
                        </c:when>
                        <c:when test="${rowRoles.groupId eq guest.roleId}">
                            <c:set var="nameRole" value="${guest.roleName}"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="nameRole" value="error"/>
                        </c:otherwise>
                    </c:choose>

                    <c:set var="rolesList" value="${rolesList}${nameRole}, "/>


                </c:forEach>
                <td><c:out value="${rolesList}"/></td>
            </tr>

            <c:remove var="rolesList" />

        </c:forEach>
    </table>
    <br>
    <br>

    <h2> Web Crawler's roles groups: </h2>

    <sql:query dataSource="${snapshot}" var="rolesGroup">
        SELECT * from RolesGroup;
    </sql:query>
    <table border="2" width="80%">
        <tr>
            <th>Roles group ID</th>
            <th>Group's name</th>
            <th>Description</th>
        </tr>
        <c:forEach var="roles" items="${rolesGroup.rows}">

            <tr>
                <td><c:out value="${roles.groupId}"/></td>
                <td><c:out value="${roles.groupName}"/></td>
                <td><c:out value="${roles.description}"/></td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <br>

</div>
<%@include file="footer.jsp" %>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 1/31/17
  Time: 5:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Management</title>
</head>
<body>

<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Management:</h4>
    <a href="nodesmanage.jsp">Manege nodes</a>
    <a href="usersmanage.jsp">Manage users</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <c:set var="roleId" value="3" />
        <c:set var="permission" value="${sessionScope.securityToken.permission}"/>

            <c:forEach var="item" items="${permission}">
            <c:if test="${item.groupId < roleId}">
            <c:set var="roleId" value="${item.groupId}" />
            </c:if>
            </c:forEach>

            Welcome ${sessionScope.securityToken.userData.userName}! <br>

            <c:choose>

                <c:when test="${roleId == 3}">

                    You don't have enough rights to use this part of the site.

                </c:when>
                <c:when test="${roleId == 2}">

                    You can use only manage node.<br>
                    But, You don't have enough rights to use user's manage.

                </c:when>
                <c:when test="${roleId == 1}">

                    You have full access!

                </c:when>
                <c:otherwise>
                    <p style="color: red" > Some thing wrong! Please re login in system!</p>
                </c:otherwise>
            </c:choose>

</div>

<%@include file="footer.jsp" %>

</body>
</html>

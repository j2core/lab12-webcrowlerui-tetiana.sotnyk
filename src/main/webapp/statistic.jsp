<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 1/31/17
  Time: 5:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Web Crawler statistics</title>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Statistics:</h4>
    <a style="background-color: lightgray" href="#">Web Crawler statistics</a>
    <a href="nodestatistics.jsp">Node's statistics</a>
    <a href="userstatistics.jsp">User's statistics</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <h2> Node's statistics: </h2>

        <sql:query dataSource="${snapshot}" var="nodes">
            SELECT * from nodeData;
        </sql:query>
        <table border="2" width="80%">
            <tr>
                <th>Node's Id</th>
                <th>Start time</th>
                <th>Stop time</th>
                <th>View statistics</th>
            </tr>
            <c:forEach var="node" items="${nodes.rows}">

                <tr>

                    <td>
                        <c:out value="${node.nodeId}"/>

                    </td>

                    <jsp:useBean id="startDate" class="java.util.Date"/>
                    <jsp:setProperty name="startDate" property="time" value="${node.startUnixTime}"/>
                    <td><fmt:formatDate value="${startDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>

                    <c:if test="${node.stopUnixTime > 0}">
                        <jsp:useBean id="stopDate" class="java.util.Date"/>
                        <jsp:setProperty name="stopDate" property="time" value="${node.stopUnixTime}"/>
                        <td><fmt:formatDate value="${stopDate}" pattern="dd/MM/yyyy hh:mm:ss"/></td>
                    </c:if>
                    <c:if test="${node.stopUnixTime == 0}">
                        <td><c:out value="00:00:00"/></td>
                    </c:if>

                    <td>
                        <form action="NodesStatisticsServlet" method="post">
                            <button type="submit" name="nodeId" value="${node.nodeId}"> view statistics</button>
                        </form>
                    </td>

                </tr>

            </c:forEach>

            <sql:query dataSource="${snapshot}" var="stopNodes">
                SELECT * FROM NodeData WHERE statusWork = 0;
            </sql:query>

            <tr style="background-color: gainsboro">
                <td><c:out value=" Total = ${nodes.rowCount}"></c:out></td>
                <td><c:out value=" total started nodes = ${nodes.rowCount}"></c:out></td>
                <td><c:out value=" total stopped nodes = ${stopNodes.rowCount}"></c:out></td>
                <td></td>
            </tr>

        </table>

        <br>
        <br>

        <h2> URL's statistics: </h2>

        <sql:query dataSource="${snapshot}" var="notProcessesUrls">
            SELECT * from urlData WHERE status = 0;
        </sql:query>

        <sql:query dataSource="${snapshot}" var="processedUrls">
            SELECT * from urlData WHERE status = 2;
        </sql:query>

        <sql:query dataSource="${snapshot}" var="urls">
            SELECT * from urlData;
        </sql:query>

        <table border="2" width="80%">
            <tr>
                <th></th>
                <th></th>
            </tr>

            <tr>

                <td><c:out value="Amount not processed URLs"></c:out></td>
                <td><c:out value="${notProcessesUrls.rowCount}"></c:out></td>

            </tr>
            <tr>

                <td><c:out value="Amount processes URLs"></c:out></td>
                <td><c:out value="${urls.rowCount - notProcessesUrls.rowCount - processedUrls.rowCount}"></c:out></td>

            </tr>
            <tr>

                <td><c:out value="Amount processed URLs"></c:out></td>
                <td><c:out value="${processedUrls.rowCount}"></c:out></td>

            </tr>
            <tr style="background-color: gainsboro">

                <td><c:out value="Total URLs"></c:out></td>
                <td><c:out value="${urls.rowCount}"></c:out></td>

            </tr>

        </table>

</div>

<%@include file="footer.jsp" %>

</body>
</html>
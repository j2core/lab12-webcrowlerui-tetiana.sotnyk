<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  Created by IntelliJ IDEA.
  User: sts
  Date: 3/20/17
  Time: 7:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin add new user</title>
</head>
<body>
<%@include file="header.jsp" %>
<%@include file="dbconnect.jsp"%>

<nav class="w3-sidenav w3-white w3-card-2" style="width:15%">
    <h4>Management:</h4>
    <a href="#">Manege nodes</a>
    <a style="background-color: lightgray" href="usersmanage.jsp">Manage users</a>
    <br>
    <a href="LogoutServlet">Logout</a>
    <a href="index.jsp">Home</a>

</nav>

<div class="w3-container" style="margin-left: 15%">

        <sql:query dataSource="${snapshot}" var="adminRole">
            SELECT * from rolesGroup WHERE groupId = 1;
        </sql:query>

        <c:set var="contains" value="false" scope="page"/>

        <c:forEach var="row" items="${adminRole.rows}">

            <c:set var="contains" value="false" scope="page"/>

            <c:forEach var="entity" items="${sessionScope.securityToken.permission}" >

                <c:if test="${entity.groupName eq row.groupName }">

                    <c:set var="contains" value="true" scope="page" />

                </c:if>

            </c:forEach>

        </c:forEach>

        <c:if test="${contains}">

            <c:if test="${sessionScope.newUser != null && sessionScope.newUser == false}">

                <p style="color: red">Something wrong with user's information or with save user's information! Please try again! </p>

            </c:if>

            <c:if test="${sessionScope.newUser == true}">

                <h4 style="color: blue">Add new user was successfully!</h4>

                ${sessionScope.remove("newUser")}

            </c:if>

            <h4> Please write information about new user:</h4>

            <form action="AdminAddNewUserServlet" method="post">

            User's name:<br>
                <input type="text" name="name" /><br>

            User's login:<br>
                <input type="text" name="login" /><br>

            User's password:<br>
                <input type="password" name="pass1" /><br>

            User's password again:<br>
                <input type="password" name="pass2" /><br>

            User's roles in APP:<br>
                <input type="checkbox" name="role" value="Guest" /> Guest
                <input type="checkbox" name="role" value="User" /> User
                <input type="checkbox" name="role" value="Admin" /> Admin<br><br>

                <input type="submit" name="submit" />
        </form>

        </c:if>

        <c:if test="${!contains}">

            <p style="color: red">You do not have enough rights to be present in this part of the site.</p>

        </c:if>

</div>

<%@include file="footer.jsp" %>
</body>
</html>

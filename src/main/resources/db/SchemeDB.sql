CREATE TABLE userData      (
                                   userId INT(11) NOT NULL AUTO_INCREMENT,
                                   login TEXT NOT NULL,
                                   userName TEXT NOT NULL,
                                   password TEXT NOT NULL,
                                   PRIMARY KEY(userId)
                                    );

CREATE TABLE rolesGroup   (
                                   groupId INT(11) NOT NULL AUTO_INCREMENT,
                                   groupName TEXT NOT NULL,
                                   description TEXT,
                                   PRIMARY KEY (groupId)
                                   );


CREATE TABLE permission   (
                                  permissionId INT(11) NOT NULL AUTO_INCREMENT,
                                  userId INT(11) NOT NULL,
                                  groupId INT(11) NOT NULL,
                                  PRIMARY KEY (userId)
                                   );

CREATE TABLE nodeData       (
                                  nodeId INT(11) NOT NULL AUTO_INCREMENT,
                                  nodeName TEXT NOT NULL,
                                  startUnixTime INTEGER,
                                  stopUnixTime INTEGER,
                                  stoppedFlag BOOL DEFAULT FALSE ,
                                  statusWork BOOL DEFAULT TRUE ,
                                  PRIMARY KEY(nodeId)
);


CREATE TABLE urlData      (
                                   urlId INT(11) NOT NULL AUTO_INCREMENT,
                                   url TEXT NOT NULL,
                                   amountTransition TINYINT,
                                   status ENUM('not_processed', 'processes', 'processed') DEFAULT 'not_processed',
                                   PRIMARY KEY(urlId)
                                    );

CREATE TABLE pageInformation    (
                                   pageId INT(11) NOT NULL AUTO_INCREMENT,
                                   urlId INT(11)NOT NULL,
                                   dateInformation DATE,
                                   pageText LONGTEXT CHARACTER SET utf8,
                                   PRIMARY KEY(pageId, urlId)
                                    );

CREATE TABLE wordInformation (
                                    wordId INT(11) NOT NULL AUTO_INCREMENT,
                                    pageId INT(11) NOT NULL,
                                    urlId INT(11) NOT NULL,
                                    word VARCHAR(254) NOT NULL,
                                    amountOnPage SMALLINT(5) NOT NULL,
                                    PRIMARY KEY(wordId, pageId, urlId)
                                     );

package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.service.CryptographerUserPassword;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 3/21/17.
 */
public class AdminAddNewUserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        String name = request.getParameter("name");
        String login = request.getParameter("login");
        String userPass1 = request.getParameter("pass1");
        String userPass2 = request.getParameter("pass2");
        String[] roles = request.getParameterValues("role");

        if (!userPass1.equals(userPass2)) {

            session.setAttribute("newUser", false);

            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/adminaddnewuser.jsp");

            requestDispatcher.include(request, response);

        } else {

            String password = CryptographerUserPassword.getSecurePassword(login + userPass1);
            WorkerDB workerDB = new WorkerDB();
            boolean result = workerDB.addNewUser(name, login, password, roles);

            if (result){
                session.setAttribute("newUser", true);
            }else {
                session.setAttribute("newUser", false);
            }

            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/adminaddnewuser.jsp");

            requestDispatcher.include(request, response);

        }
    }
}


package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;
import com.j2core.sts.webcrawler.ui.service.CryptographerUserPassword;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by sts on 12/12/16.
 */
public class LoginServlet extends HttpServlet{

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        String login = request.getParameter("login");
        String userPass = request.getParameter("password");
        String password = CryptographerUserPassword.getSecurePassword(login + userPass);
        WorkerDB workerDB = new WorkerDB();
        SecurityToken securityToken = workerDB.getSecurityToken(login, password);

        if (securityToken !=  null) {

            HttpSession session = request.getSession();
            session.setAttribute("securityToken", securityToken);
            session.setAttribute("verifyLogin", true);
            //setting session to expiry in 30 mins
            session.setMaxInactiveInterval(30*60);

            response.sendRedirect("index.jsp");

        }else {

            HttpSession session = request.getSession();
            session.setAttribute("verifyLogin", false);

            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/index.jsp");

            requestDispatcher.include(request, response);
        }
    }
}

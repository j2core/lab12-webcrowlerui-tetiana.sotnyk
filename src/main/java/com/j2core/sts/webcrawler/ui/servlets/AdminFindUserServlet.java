package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 3/22/17.
 */
public class AdminFindUserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        String jspPage = request.getParameter("jspPage");
        String userId = request.getParameter("userId");
        String login = request.getParameter("login");
        String name = request.getParameter("name");

        HttpSession session = request.getSession();
        WorkerDB workerDB = new WorkerDB();


        SecurityToken userToken = workerDB.findUserInformation(userId, login, name);

        if (userToken != null) {

            session.setAttribute("userToken", userToken);

        } else {

            session.setAttribute("exception", true);

        }

        response.sendRedirect(jspPage);

    }
}

package com.j2core.sts.webcrawler.ui.servlets;

import com.j2core.sts.webcrawler.ui.dao.WorkerDB;
import com.j2core.sts.webcrawler.ui.dto.SecurityToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sts on 3/23/17.
 */
public class AdminChangeUserDataServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        WorkerDB workerDB = new WorkerDB();

        String name = request.getParameter("name");
        String login = request.getParameter("login");
        String pass1 = request.getParameter("pass1");
        String pass2 = request.getParameter("pass2");
        String[] roles = request.getParameterValues("role");
        SecurityToken userToken = (SecurityToken) session.getAttribute("userToken");
        String password = "";

        if (pass1.equals(pass2) && ((login.length() > 0 && pass1.length() > 0) || (login.length() < 1 && pass1.length() < 1))) {

            SecurityToken token = workerDB.adminChangeUserInformation(userToken, name, login, password, roles);

            if (token != null) {

                session.setAttribute("userToken", token);
                session.setAttribute("alteredUser", true);

            } else {

                session.setAttribute("exception", true);

            }
        }else {

            session.setAttribute("exception", true);

        }
        response.sendRedirect("adminchangeuserdata.jsp");

    }
}

package com.j2core.sts.webcrawler.ui.dao;

import com.j2core.sts.webcrawler.ui.dto.*;
import com.j2core.sts.webcrawler.ui.service.CryptographerUserPassword;
import org.apache.log4j.Logger;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 1/20/17.
 */
public class WorkerDB {

    private final static Logger LOGGER = Logger.getLogger(WorkerDB.class); // class for save logs information
    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("webCrawler");

    public WorkerDB() {
    }

    public SecurityToken getSecurityToken(String login, String pass) {

        SecurityToken token = null;
        UserData userData;

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            userData = (UserData) entityManager.createQuery("select e from UserData e where e.login = :login and e.password = :password").
                    setParameter("login", login).setParameter("password", pass).getSingleResult();

            token = new SecurityToken();
            token.setUserData(userData);

            List<RolesGroup> groupList = findUserRole(userData);

            if (groupList != null) {
                token.setPermission(groupList);
            }

        } catch (NoResultException ex) {

            return token;
        } finally {
            entityManager.close();
        }

        return token;

    }


    public boolean changeUserInformation(UserData userData, String newUserName, String newLogin, String newPass) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            entityManager.getTransaction().begin();

            userData.setUserName(newUserName);
            userData.setLogin(newLogin);
            userData.setPassword(newPass);
            entityManager.merge(userData);

            entityManager.getTransaction().commit();

        } catch (Exception ex) {

            return false;

        } finally {
            entityManager.close();
        }
        return true;

    }


    public boolean addNewUser(String name, String login, String pass, String[] roles){

        UserData userData = new UserData(login, name, pass);

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            entityManager.getTransaction().begin();

            entityManager.persist(userData);

            for (String role : roles){

                int roleId = UserRole.fineRoleId(role);
                RolesGroup rolesGroup = entityManager.find(RolesGroup.class, roleId);

                Permission permission = new Permission(userData, rolesGroup);

                entityManager.persist(permission);

            }

            entityManager.getTransaction().commit();

        } catch (Exception ex) {

            return false;

        } finally {
            entityManager.close();
        }

       return true;
    }


    public SecurityToken findUserInformation(String userId, String login, String name){

        SecurityToken userToken = null;

        UserData user = findUser(userId, login, name);

        if (user != null){

            userToken = new SecurityToken();
            userToken.setUserData(user);

            List<RolesGroup> groupList = findUserRole(user);

            if (groupList != null) {
                userToken.setPermission(groupList);
            }

        }

        return userToken;
    }


    private UserData findUser(String userId, String login, String name){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        UserData user = null;

        try {
            if (userId.length() > 0){

                int id = Integer.parseInt(userId);

                user = entityManager.find(UserData.class, id);

            }else if(login.length() > 0){

                user = (UserData) entityManager.createQuery("select e from UserData e where e.login = :login").
                        setParameter("login", login).getSingleResult();

            }else if (name.length() > 0){

                user = (UserData) entityManager.createQuery("select e from UserData e where e.userName = :userName").
                        setParameter("userName", name).getSingleResult();

            }
        } catch (Exception ex) {

            return null;

        } finally {
            entityManager.close();
        }

        return user;

    }

    private List<RolesGroup> findUserRole(UserData user){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<RolesGroup> groupList = null;

        try {

            List<Permission> permissionList = entityManager.createQuery("select e from Permission e where e.userData = :userId").
                    setParameter("userId", user).getResultList();

            if (!permissionList.isEmpty()){

                groupList = new LinkedList<RolesGroup>();
                for (Permission permission : permissionList){

                    groupList.add(entityManager.find(RolesGroup.class, permission.getRolesGroup().getGroupId()));

                }

            }
        } catch (Exception ex) {

            return null;

        } finally {
            entityManager.close();
        }

        return groupList;
    }


    public boolean deleteUser(SecurityToken userToken){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        boolean result = false;
        UserData userData = userToken.getUserData();

        try {

            entityManager.getTransaction().begin();

            int amountDeletedPermission = entityManager.createQuery("delete from Permission e where e.userData = :userId").
                    setParameter("userId", userData).executeUpdate();

            if (amountDeletedPermission == userToken.getPermission().size()){

                int id = userData.getUserId();

                UserData user = entityManager.find(UserData.class, id);
                entityManager.remove(user);
                result = true;

            }

            entityManager.getTransaction().commit();

        } catch (Exception ex) {

            return result;

        } finally {
            entityManager.close();
        }

        return result;

    }


    public SecurityToken adminChangeUserInformation(SecurityToken token, String newName, String newLogin, String newPass, String[] newRoles){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        SecurityToken userToken = null;
        UserData userData = token.getUserData();
        List<RolesGroup> rolesGroups = token.getPermission();

        try {

            entityManager.getTransaction().begin();

            if (newName.length() > 0) userData.setUserName(newName);

            if (newLogin.length() > 0) {

                userData.setLogin(newLogin);
                userData.setPassword(CryptographerUserPassword.getSecurePassword(newLogin + newPass));

            }

            entityManager.merge(userData);

            entityManager.getTransaction().commit();

            userToken = new SecurityToken();
            userToken.setUserData(userData);
            userToken.setPermission(changeUserRole(entityManager, token.getUserData(), userData, rolesGroups, newRoles));

        } catch (Exception ex) {

            return userToken;

        } finally {
            entityManager.close();
        }


        return userToken;

    }


    private List<RolesGroup> changeUserRole(EntityManager entityManager, UserData oldUser, UserData newUser, List<RolesGroup> token, String[] roles){

        List<RolesGroup> newRoles = null;

        try {

            entityManager.getTransaction().begin();

            List<Permission> userPermission  = entityManager.createQuery("select e from Permission e where e.userData = :userId").
                    setParameter("userId", oldUser).getResultList();

            if (roles.length > 0){

                newRoles = new LinkedList<RolesGroup>();

                Permission permission;
                RolesGroup role;
                int roleId;
                int index = 0;
                while (index < roles.length){

                    if (!userPermission.isEmpty()){

                        permission = userPermission.remove(0);
                        roleId = UserRole.fineRoleId(roles[index]);
                        role = entityManager.find(RolesGroup.class, roleId);

                        permission.setUserData(newUser);
                        permission.setRolesGroup(role);

                        entityManager.merge(permission);

                        newRoles.add(role);

                    }else {

                        roleId = UserRole.fineRoleId(roles[index]);
                        role = entityManager.find(RolesGroup.class, roleId);
                        permission = new Permission(newUser, role);

                        entityManager.persist(permission);
                    }

                    index++;

                }

                if (!userPermission.isEmpty()){

                    for (Permission perm : userPermission){

                        entityManager.remove(perm);

                    }
                }

            }else {

                for (Permission perm : userPermission){

                    entityManager.remove(perm);

                }
            }

            entityManager.getTransaction().commit();

        }catch (Exception ex){

            return null;

        }

        return newRoles;
    }


    public boolean stopNode(int nodeId){

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            NodeData nodeData = entityManager.find(NodeData.class, nodeId);

            nodeData.setStoppedFlag(true);

            entityManager.getTransaction().begin();

            entityManager.merge(nodeData);

            entityManager.getTransaction().commit();

        }catch (Exception ex){
            LOGGER.error(ex);
            return false;
        }finally {
            entityManager.close();
        }

        return true;

    }

}

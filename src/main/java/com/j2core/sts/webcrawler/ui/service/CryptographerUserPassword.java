package com.j2core.sts.webcrawler.ui.service;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by sts on 1/20/17.
 */
public class CryptographerUserPassword {

    private static final String salt = "alisa";

    public static String getSecurePassword(String passwordToHash) {

        return DigestUtils.sha512Hex(passwordToHash + salt);

    }
}

package com.j2core.sts.webcrawler.ui.dto;

/**
 * Created by sts on 3/21/17.
 */
public enum UserRole {

    GUEST("Guest", 3),
    USER("User", 2),
    ADMIN("Admin", 1);

    private final String roleName;
    private final int roleId;

    UserRole(String roleName, int roleId) {
        this.roleName = roleName;
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public int getRoleId() {
        return roleId;
    }

    public static int fineRoleId(String name){

        for(UserRole role : UserRole.values()){

            if (name.equals(role.getRoleName())){
                return role.getRoleId();
            }
        }
        return -1;
    }
}
